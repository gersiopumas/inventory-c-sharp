﻿using System.Windows.Forms;
using System.Drawing;
namespace c_
{
  partial class Form1
  {
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private TextBox ProductInput;
    private TextBox NameColumnInput;
    private TextBox QtyColumnInput;
    private CheckBox IgnoreHeader;
    private Panel ProductPanel;
    private Panel QuantityPanel;
    private Panel CountPanel;
    private TableLayoutPanel CollationPanel;
    private ListBox ProductList;
    private ListBox QuantityList;
    private Button CreateLog;
    private Button ResetButton;
    private ListBox CountList;
    private Label NameKeeperLabel;
    private Label OrderNumberLabel;
    private Label DeliveryLabel;
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(900, 700);
      this.Text = "Cotejacion Inventario";

      this.ProductPanel = new Panel();
      this.QuantityPanel = new Panel();
      this.CountPanel = new Panel();
      this.CollationPanel = new TableLayoutPanel();
      this.CollationPanel.RowCount = 5;
      this.CollationPanel.ColumnCount = 2;

      this.CollationPanel.Dock = DockStyle.Fill;
      this.ProductPanel.Dock = DockStyle.Fill;
      this.QuantityPanel.Dock = DockStyle.Fill;
      this.CountPanel.Dock = DockStyle.Fill;

      this.ProductInput = new TextBox();
      this.NameColumnInput = new TextBox();
      this.QtyColumnInput = new TextBox();
      this.IgnoreHeader = new CheckBox();

      Label ItemCodeLabel = new Label();
      Label ColumnNameLabel = new Label();
      Label QtyNameLabel = new Label();
      this.NameKeeperLabel = new Label();
      this.OrderNumberLabel = new Label();
      this.DeliveryLabel = new Label();
      this.NameKeeperLabel.Text = "Encargado: ";
      this.OrderNumberLabel.Text = "Orden: ";
      this.DeliveryLabel.Text = "Delivery: ";
      Font boldFont = new Font("Arial", 12, FontStyle.Bold);
      this.NameKeeperLabel.Font = boldFont;
      this.OrderNumberLabel.Font = boldFont;
      this.DeliveryLabel.Font = boldFont;
      this.NameKeeperLabel.Dock = DockStyle.Fill;
      this.OrderNumberLabel.Dock = DockStyle.Fill;
      this.DeliveryLabel.Dock = DockStyle.Fill;

      ItemCodeLabel.Text = "Código a escanear:";
      ColumnNameLabel.Text = "# de columna del archivo que contiene los códigos:";
      QtyNameLabel.Text = "# de columna del archivo que contiene las cantidades:";
      this.IgnoreHeader.Text = "Ignorar primera línea de archivo (encabezado)";

      ItemCodeLabel.Dock = DockStyle.Fill;
      ColumnNameLabel.Dock = DockStyle.Fill;
      QtyNameLabel.Dock = DockStyle.Fill;
      this.IgnoreHeader.Dock = DockStyle.Fill;

      this.IgnoreHeader.Checked = true;

      this.NameColumnInput.Text = "2";
      this.QtyColumnInput.Text = "5";

      this.ProductInput.Dock = DockStyle.Fill;
      this.NameColumnInput.Dock = DockStyle.Fill;
      this.QtyColumnInput.Dock = DockStyle.Fill;
      this.CollationPanel.Height = 110;
      this.CollationPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
      this.CollationPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));

      this.CollationPanel.Controls.Add(ItemCodeLabel, 0, 1);
      this.CollationPanel.Controls.Add(this.ProductInput, 0, 2);
      this.CollationPanel.Controls.Add(this.IgnoreHeader, 1, 2);

      this.CollationPanel.Controls.Add(ColumnNameLabel, 0, 3);
      this.CollationPanel.Controls.Add(QtyNameLabel, 1, 3);

      this.CollationPanel.Controls.Add(this.NameColumnInput, 0, 4);
      this.CollationPanel.Controls.Add(this.QtyColumnInput, 1, 4);

      this.ProductList = new ListBox();
      this.QuantityList = new ListBox();
      this.CountList = new ListBox();
      this.ProductList.Dock = DockStyle.Fill;
      this.QuantityList.Dock = DockStyle.Fill;
      this.CountList.Dock = DockStyle.Fill;
      this.ProductList.IntegralHeight = false;
      this.QuantityList.IntegralHeight = false;
      this.CountList.IntegralHeight = false;
      this.ProductList.Height = 400;
      this.QuantityList.Height = 400;
      this.CountList.Height = 400;

      this.ProductPanel.Controls.Add(this.ProductList);
      this.QuantityPanel.Controls.Add(this.QuantityList);
      this.CountPanel.Controls.Add(this.CountList);

      this.CreateLog = new Button();
      this.CreateLog.Text = "Crear reporte .txt";
      this.CreateLog.Height = 50;
      this.ResetButton = new Button();
      this.ResetButton.Text = "Leer nuevo documento";
      this.ResetButton.Height = 50;
      this.ResetButton.Margin = new Padding(0, 0, 0, 0);
      this.ResetButton.Dock = DockStyle.Bottom;
      this.CreateLog.Dock = DockStyle.Bottom;
      TableLayoutPanel InfoPanel = new TableLayoutPanel();
      InfoPanel.RowCount = 1;
      InfoPanel.ColumnCount = 3;
      InfoPanel.Dock = DockStyle.Fill;
      InfoPanel.Height = 40;
      InfoPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33F));
      InfoPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33F));
      InfoPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33F));

      InfoPanel.Controls.Add(this.NameKeeperLabel, 0, 1);
      InfoPanel.Controls.Add(this.OrderNumberLabel, 1, 1);
      InfoPanel.Controls.Add(this.DeliveryLabel, 2, 1);
      TableLayoutPanel RootPanel = new TableLayoutPanel();
      RootPanel.RowCount = 4;
      RootPanel.ColumnCount = 1;
      RootPanel.Dock = DockStyle.Fill;
      RootPanel.Padding = new Padding(5);

      TableLayoutPanel ColumnPanel = new TableLayoutPanel();
      ColumnPanel.Dock = DockStyle.Fill;
      ColumnPanel.ColumnCount = 3;
      ColumnPanel.RowCount = 1;
      ColumnPanel.Height = 400;
      ColumnPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 30F));
      ColumnPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 30F));
      ColumnPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 30F));
      ColumnPanel.Controls.Add(this.ProductPanel, 0, 1);
      ColumnPanel.Controls.Add(this.QuantityPanel, 1, 1);
      ColumnPanel.Controls.Add(this.CountPanel, 2, 1);

      RootPanel.Controls.Add(InfoPanel, 0, 0);
      RootPanel.Controls.Add(CollationPanel, 0, 1);
      RootPanel.Controls.Add(ColumnPanel, 0, 2);
      RootPanel.Controls.Add(this.CreateLog, 0, 3);
      RootPanel.Controls.Add(this.ResetButton, 0, 3);

      this.Controls.Add(RootPanel);

    }

    #endregion
  }
}


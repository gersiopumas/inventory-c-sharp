﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Media;
using System.IO;
using ExcelDataReader;
using Essy.Tools.InputBox;
namespace c_
{
  public class Product
  {
    public string Name { get; set; }
    public int Quantity { get; set; }
    public int Count { get; set; }
    public int isValid()
    {
      if (this.Quantity == this.Count)
      {
        return 0;
      }
      return this.Quantity > this.Count ? 1 : -1;
    }

    public void AddToCount()
    {
      this.Count += 1;
    }
    public void DecreaseToCount()
    {
      if (this.Count > 0)
      {
        this.Count -= 1;
      }
    }
  }
  public partial class Form1 : Form
  {
    private List<Product> Products;
    private string DraggedFile;
    private string _selectedMenuItem;
    private readonly ContextMenuStrip collectionRoundMenuStrip;
    private List<string> Action_Log;
    private List<string> Bad_Item_log;
    private List<string> Error_log;
    private List<string> Reader_log;
    private DateTime Start_Scan;
    private DateTime End_Scan;
    private string OrderNumber;
    public Form1()
    {
      InitializeComponent();
      // Set the start position of the form to the center of the screen.
      this.StartPosition = FormStartPosition.CenterScreen;
      // setup Variables
      this.Products = new List<Product>();
      this.Action_Log = new List<string>();
      this.Bad_Item_log = new List<string>();
      this.Error_log = new List<string>();
      this.Reader_log = new List<string>();
      // Setup windows style
      this.FormBorderStyle = FormBorderStyle.FixedSingle;
      this.MaximizeBox = true;
      this.MinimizeBox = true;
      // Setup Search Event Handler
      this.ProductInput.KeyDown += new KeyEventHandler(this.On_Search_Event);

      //Setup OnDrag Event
      this.AllowDrop = true;
      this.DragEnter += new DragEventHandler(Form_DragEnter);
      this.DragDrop += new DragEventHandler(Form1_DragDrop);

      // Setup List 
      this.ProductList.DrawMode = DrawMode.OwnerDrawFixed;
      this.QuantityList.DrawMode = DrawMode.OwnerDrawFixed;
      this.CountList.DrawMode = DrawMode.OwnerDrawFixed;
      this.ProductList.DrawItem += new DrawItemEventHandler(listBox_DrawItem);
      this.QuantityList.DrawItem += new DrawItemEventHandler(listBox_DrawItem);
      this.CountList.DrawItem += new DrawItemEventHandler(listBox_DrawItem);
      this.ProductList.ItemHeight = 20;
      this.QuantityList.ItemHeight = 20;
      this.CountList.ItemHeight = 20;
      // Setup onScroll Syncronization 
      EventHandler handler = (s, e) =>
      {
        if (s == this.ProductList)
        {
          this.QuantityList.TopIndex = this.ProductList.TopIndex;
          this.CountList.TopIndex = this.ProductList.TopIndex;
        }
        if (s == this.QuantityList)
        {
          this.ProductList.TopIndex = this.QuantityList.TopIndex;
          this.CountList.TopIndex = this.QuantityList.TopIndex;
        }
        if (s == this.CountList)
        {
          this.QuantityList.TopIndex = this.CountList.TopIndex;
          this.ProductList.TopIndex = this.CountList.TopIndex;
        }
      };
      this.ProductList.MouseCaptureChanged += handler;
      this.QuantityList.MouseCaptureChanged += handler;
      this.CountList.MouseCaptureChanged += handler;
      this.ProductList.SelectedIndexChanged += handler;
      this.QuantityList.SelectedIndexChanged += handler;
      this.CountList.SelectedIndexChanged += handler;

      // Context Menu
      var toolStripMenuItem1 = new ToolStripMenuItem { Text = "Copiar codigo" };
      var toolStripMenuItem2 = new ToolStripMenuItem { Text = "Decrementar" };
      toolStripMenuItem1.Click += toolStripMenuItem1_Click;
      toolStripMenuItem2.Click += toolStripMenuItem2_Click;
      this.collectionRoundMenuStrip = new ContextMenuStrip();
      this.collectionRoundMenuStrip.Items.AddRange(new ToolStripItem[] { toolStripMenuItem1, toolStripMenuItem2 });
      this.ProductList.MouseDown += On_Right_Click_Event;
      this.ProductList.ContextMenuStrip = this.collectionRoundMenuStrip;
      // Create report
      this.CreateLog.Click += new EventHandler(this.WriteLog);
      this.ResetButton.Click += new EventHandler(this.resetVariable);
    }
    // drag Handler
    void Form_DragEnter(object sender, DragEventArgs e)
    {
      // Check if the Dataformat of the data can be accepted
      // (we only accept file drops from Explorer, etc.)
      if (e.Data.GetDataPresent(DataFormats.FileDrop))
        e.Effect = DragDropEffects.Copy; // Okay
      else
        e.Effect = DragDropEffects.None; // Unknown data, ignore it
    }
    // Drag and drop Handler
    void Form1_DragDrop(object sender, DragEventArgs e)
    {
      // Can support multiple files, implementation only take the last file as the readed.
      string[] files = (string[])e.Data.GetData(DataFormats.FileDrop, false);
      foreach (string file in files) this.DraggedFile = file;
      // Call read excel file.
      this.Start_Scan = DateTime.Now;
      this.AskAndSaveName();
      this.AskAndSaveDelivery();
      this.readExcel();
    }
    // Search Item On List
    private void On_Search_Event(object sender, KeyEventArgs e)
    {
      //enter key is down and product is readed
      if (e.KeyCode == Keys.Enter && this.ProductInput.TextLength > 0)
      {
        this.Action_Log.Add("Busqueda:" + this.ProductInput.Text);
        Product temp = new Product();
        // get the first 2 characters
        string checkP1 = this.ProductInput.Text.Substring(0, 2);
        // if 
        if (checkP1.Equals("1P"))
        {
          string RemoveFirstTwoChars = this.ProductInput.Text.Substring(2);
          temp = this.Products.Find(product => product.Name.Equals(RemoveFirstTwoChars));
        }
        else
        {
          temp = this.Products.Find(product => product.Name.Equals(this.ProductInput.Text));
        }
        if (temp != null)
        {
          this.Action_Log.Add("Aumentar conteo:" + this.ProductInput.Text);
          temp.AddToCount();
          this.playsound(true);
        }
        else
        {
          this.playsound(false);
          this.Bad_Item_log.Add("El artículo (" + this.ProductInput.Text + ") no está en el archivo de excel");
          MessageBox.Show(
            "El artículo escaneado no está en el archivo de excel",
            "Error",
            MessageBoxButtons.OK,
            MessageBoxIcon.Error
          );
        }
        this.UpdateList();
        this.ProductInput.Text = "";
        e.Handled = true;
        e.SuppressKeyPress = true;
      }
    }
    // Read Excel File
    private void readExcel()
    {
      try
      {
        // Clear original array
        this.Products.Clear();
        // Clear List // Remove the next line if u want to concat multiple files.
        this.CleanList();
        // Text-encoding
        this.Action_Log.Add("Lectura de archivo:" + this.DraggedFile);

        System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);
        using (var stream = File.Open(this.DraggedFile, FileMode.Open, FileAccess.Read))
        {
          // excel reader
          using (var reader = ExcelReaderFactory.CreateReader(stream))
          {
            do
            {
              int i = 0;
              while (reader.Read()) //Each ROW
              {
                // if first row are headers ignore it
                if (this.IgnoreHeader.Checked && i == 0)
                {
                  reader.Read();
                }
                Product SingleProduct = new Product();
                // to improve probabbly for cycle not necessary.
                for (int column = 0; column < reader.FieldCount; column++) // Fill Product object from each column.
                {
                  Object obj = reader.GetValue(column);
                  if (column == 2)
                  {
                    this.OrderNumber = obj.ToString();
                    this.OrderNumberLabel.Text = "Orden: " + this.OrderNumber;
                  }
                  if (column == Int16.Parse(this.NameColumnInput.Text) - 1)
                  {
                    SingleProduct.Name = obj.ToString();
                  }
                  if (column == Int16.Parse(this.QtyColumnInput.Text) - 1)
                  {
                    SingleProduct.Quantity = Int16.Parse(obj.ToString());
                  }
                }
                SingleProduct.Count = 0; // initial Count is Zero
                this.Products.Add(SingleProduct); // Add to array.
                i++; // update row.
              }
            } while (reader.NextResult()); //Move to NEXT SHEET
          }
        }
        // Update List
        this.UpdateList();
      }
      catch (Exception ex)
      {
        Error_log.Add(ex.GetBaseException().ToString());
        // Show error Box.
        MessageBox.Show(
          ex.GetBaseException().ToString(),
          "Error",
          MessageBoxButtons.OK,
          MessageBoxIcon.Error
        );
      }
    }
    // Update ListBoxes
    private void UpdateList()
    {
      this.CleanList();
      foreach (Product p in this.Products)
      {
        this.ProductList.Items.Add(p.Name);
        this.QuantityList.Items.Add(p.Quantity);
        this.CountList.Items.Add(p.Count);
      }
    }
    // Clean ListBoxes
    private void CleanList()
    {
      this.ProductList.Items.Clear();
      this.QuantityList.Items.Clear();
      this.CountList.Items.Clear();
    }
    private void BuildCustomLog()
    {
      // Filter products
      List<Product> success = this.Products.FindAll(product => product.isValid() == 0);
      List<Product> excess = this.Products.FindAll(product => product.isValid() == -1);
      List<Product> lack = this.Products.FindAll(product => product.isValid() == 1);
      this.End_Scan = DateTime.Now;
      this.Reader_log.Add("Orden #:" + this.OrderNumber);
      this.Reader_log.Add(this.NameKeeperLabel.Text);
      this.Reader_log.Add(this.DeliveryLabel.Text);
      this.Reader_log.Add("Fecha y hora scaneo inicio: " + this.Start_Scan.ToString());
      this.Reader_log.Add("Fecha y hora scaneo final: " + this.End_Scan.ToString());
      this.Reader_log.Add("================ INVENTARIO CORRECTO ================");
      foreach (Product p in success)
        this.Reader_log.Add(p.Name + " ✔ scanneados:" + p.Count);

      this.Reader_log.Add("================ INVENTARIO A FAVOR ================");
      foreach (Product p in excess)
        this.Reader_log.Add(p.Name + " ✘ scanneados:" + p.Count + "Sobran: " + (p.Count - p.Quantity).ToString());

      this.Reader_log.Add("================ INVENTARIO FALTANTE ================");
      foreach (Product p in lack)
        this.Reader_log.Add(p.Name + " ✘ scanneados:" + p.Count + "Faltan: " + (p.Quantity - p.Count).ToString());

      this.Reader_log.Add("== INVENTARIO ESCANEADO Y NO LOCALIZADO EN ARCHIVO ==");
      foreach (string bad_item in this.Bad_Item_log)
      {
        this.Reader_log.Add(bad_item);
      }
      this.Reader_log.Add("================ ACCIONES DE SISTEMA ===============");
      foreach (string log in this.Action_Log)
      {
        this.Reader_log.Add(log);
      }
      this.Reader_log.Add("================ ERRORES DE SISTEMA ================");
      foreach (string error_item in this.Error_log)
      {
        this.Reader_log.Add(error_item);
      }
    }
    private void resetVariable(object sender, EventArgs e)
    {
      this.OrderNumber = "";
      this.Products.Clear();
      this.Action_Log.Clear();
      this.Bad_Item_log.Clear();
      this.Error_log.Clear();
      this.Reader_log.Clear();
      this.CleanList();
    }
    private void WriteLog(object sender, EventArgs e)
    {
      // Build log on background
      this.BuildCustomLog();
      // write to file
      string docPath =
               Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

      // Write the string array to a new file named "WriteLines.txt".
      using (StreamWriter outputFile = new StreamWriter(Path.Combine(docPath, this.OrderNumber + ".txt")))
      {
        foreach (string log in this.Reader_log)
        {
          outputFile.WriteLine(log);
        }
      }
      MessageBox.Show("Escribiendo " + this.OrderNumber + ".txt en " + docPath);
    }
    // Custom ListBoxItems (colores)
    void listBox_DrawItem(object sender, DrawItemEventArgs e)
    {
      if (e.Index > -1)
      {
        Product p = Products.ElementAt(e.Index);
        e.DrawBackground();
        if (p.Count != 0)
        {
          // Validate List and select color for each validation 0 match +1 lack -1 excess.
          int validation = p.isValid();
          if (validation == 0)
          {
            e.Graphics.FillRectangle(Brushes.Green, e.Bounds);
          }
          else
          {
            if (validation == -1)
            {
              e.Graphics.FillRectangle(Brushes.Yellow, e.Bounds);
            }
            else
            {
              e.Graphics.FillRectangle(Brushes.Red, e.Bounds);
            }
          }
        }

        using (Brush textBrush = new SolidBrush(e.ForeColor))
        {
          //e.Graphics.DrawIconUnstretched(YourProject.Properties.Resources.YouIcon, rect);
          e.Graphics.DrawString(
            (sender as ListBox).Items[e.Index].ToString(),
            e.Font,
            textBrush,
            e.Bounds,
            StringFormat.GenericDefault
            );
        }
      }
    }

    // Copy Command Context Menu
    private void toolStripMenuItem1_Click(object sender, EventArgs e)
    {
      this.Action_Log.Add("Copiar:" + _selectedMenuItem);
      Clipboard.SetText(_selectedMenuItem);
    }

    // Decrease Command Context Menu
    private void toolStripMenuItem2_Click(object sender, EventArgs e)
    {
      this.Action_Log.Add("Decrementar conteo:" + _selectedMenuItem);

      Product temp = this.Products.Find(product => product.Name.Equals(this._selectedMenuItem));
      if (temp != null)
      {
        temp.DecreaseToCount();
      }
      this.UpdateList();
    }
    // Right Click Context Menu on Item
    private void On_Right_Click_Event(object sender, MouseEventArgs e)
    {
      if (e.Button != MouseButtons.Right) return;
      var index = ProductList.IndexFromPoint(e.Location);
      if (index > -1)
      {
        _selectedMenuItem = this.ProductList.Items[index].ToString();
        this.collectionRoundMenuStrip.Show(Cursor.Position);
        this.collectionRoundMenuStrip.Visible = true;
      }
      else
      {
        this.collectionRoundMenuStrip.Visible = false;
      }
    }

    private void playsound(bool isValid)
    {
      if (!isValid)
      {
        System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
        System.IO.Stream s = a.GetManifestResourceStream("c_.resources.Error.wav");
        SoundPlayer simpleSound = new SoundPlayer(s);
        simpleSound.Play();
      }
      else
      {
        System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
        System.IO.Stream s = a.GetManifestResourceStream("c_.resources.Valid.wav");
        SoundPlayer simpleSound = new SoundPlayer(s);
        simpleSound.Play();
      }
    }

    private void AskAndSaveName()
    {
      string input;
      do
      {
        input = InputBox.ShowInputBox("Nombre encargado en turno");
      } while (input == null || input == "");
      this.NameKeeperLabel.Text = "Encargado: " + input;
    }
    private void AskAndSaveDelivery()
    {
      string input;
      do
      {
        input = InputBox.ShowInputBox("Número de entrega");
      } while (input == null || input == "");
      this.DeliveryLabel.Text = "Delivery: " + input;
    }
  }


}
